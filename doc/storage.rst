Storage
=======
Requirements

1. Chunking is required to minimize amount of data read for a specific period of
   time. When using proper storage system, a chunk of data can be lost, but
   total data should be minimized.
2. Compression minimizes storage requirements. Most of the data is rarely read
   and there is no need to keep it uncompressed. Compression improves
   performance of I/O and might increase overall system performance if CPU can
   compress/decompress faster than system can write/read uncompressed data.
3. Single writer, multiple readers use case needs to be supported without
   enforcing order of starting of writer and readers. For example, as of today,
   HDF5 requires writer to start before readers, which makes it unsuitable
   storage solution.
4. Autorecovery after improper shutdown should happen without manual
   intervention. Certain data loss is acceptable, i.e. last chunk.
