File Format
===========
The data is saved in HDF files.

Sensor data is stored in sensor data group. Sensor data group can be the
root group of HDF file. If there are multiple sets of sensors, then data of
each set can be stored in separate HDF file group.

The structure of sensor data group is

1. Attributes

   version
    Integer number indicating version of the format.
   start
    Origin of sensor data reads (UTC timestamp). Origin and indices of
    `data` dataset are used to calculate timestamp of sensor value read.
   interval
    Time interval of sensor data reads.

2. Dataset for each sensor stores values read from sensor. Use origin of
   the sensor data group and indices of the dataset to calculate timestamp
   of sensor value. The dataset has `fillvalue` attribute, which indicates
   possible missing value.
3. Group can contain `_debug_` subgroup. It contains debug data used only
   for development and debugging purposes.


Debug Group
-----------
The debug group contains multiple datasets

clock_time
    Stores monotonic clock time value of corresponding value read from
    sensor and stored in sensor dataset.
read_time
    Stores UTC timestamp of corresponding value read from sensor and stored
    in sensor dataset.
duration
    Total duration of all sensors data reading and their workflow
    execution.

.. vim: sw=4:et:ai
