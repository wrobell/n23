#!/usr/bin/env python3
#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Create N23 database.
"""

import asyncio
import argparse
import logging
from urllib.parse import urlparse

from n23.storage.tdb.core import to_db_uri, execute

logger = logging.getLogger(__name__)

def execute_cmd(db_uri, cmd):
    task = execute(db_uri, cmd, enable_tx=False)
    loop.run_until_complete(task)

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', action='store_true', default=False,
    help='Explain what is being done',
)
parser.add_argument(
    '-e', '--extensions', nargs='+', default=[],
    help='List of database extensions to install (on top of TimescaleDB)'
)
parser.add_argument(
    'db', help='Database name or database connection string'
)

args = parser.parse_args()

level = logging.DEBUG if args.verbose else logging.WARN
logging.basicConfig(level=level)

loop = asyncio.get_event_loop()


db_uri = to_db_uri(args.db)
db_uri_tp1 = to_db_uri(args.db, alt_db='template1')

logger.info('db uri: {}'.format(db_uri))
logger.info('template1 db uri: {}'.format(db_uri_tp1))

dbname = urlparse(db_uri).path[1:]
cmd = 'create database "{}"'.format(dbname)
execute_cmd(db_uri_tp1, cmd)

# install mandatory timescaledb and optional extensions
cmd_ext = 'create extension if not exists {}'.format
extensions = ['timescaledb']
extensions.extend(args.extensions)
for ext in extensions:
    execute_cmd(db_uri, cmd_ext(ext))

cmd = 'alter database "{}" set timescaledb.telemetry_level=off'.format(dbname)
execute_cmd(db_uri, cmd)

# vim: sw=4:et:ai
