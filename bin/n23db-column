#!/usr/bin/env python3
#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Create N23 database table.
"""

import asyncio
import argparse
import logging

from n23.storage.tdb.core import to_db_uri, execute

SQL_ADD_COLUMN = 'alter table {table} add column {name} {type} {nullable}'

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', action='store_true', default=False,
    help='Explain what is being done',
)
parser.add_argument(
    '--nullable', action='store_true', default=False,
    help='Make column nullable',
)
parser.add_argument(
    'db', help='Database name or database connection string'
)
parser.add_argument('table', help='Table name')
parser.add_argument('column', help='Column name')
parser.add_argument('type', help='Column type')

args = parser.parse_args()

level = logging.DEBUG if args.verbose else logging.WARN
logging.basicConfig(level=level)

db_uri = to_db_uri(args.db)
nullable = '' if args.nullable else 'not null'
cmd = SQL_ADD_COLUMN.format(
    table=args.table,
    name=args.column,
    type=args.type,
    nullable=nullable,
)
logger.info('database: {}'.format(db_uri))
asyncio.run(execute(db_uri, cmd))

# vim: sw=4:et:ai

