.PHONY=check test

check:
	mypy examples/n23-load-avg
	mypy examples/n23-sensor-tag
	mypy bin/n23db-db
	mypy bin/n23db-column
	mypy bin/n23db-table

test:
	pytest -vv --cov=n23
