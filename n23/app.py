#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncio
import inspect
import logging
import os.path
import signal
import sys
import typing as tp
import uvloop
from contextlib import contextmanager
from dataclasses import dataclass, field
from urllib.parse import urlparse

from .error import ApplicationError
from .scheduler import Scheduler

logger = logging.getLogger(__name__)

class SupportsClose(tp.Protocol):
    def close(self) -> None:
        ...

@dataclass(frozen=True)
class Context:
    """
    Context for applications based on N23 framework.
    """
    scheduler: Scheduler
    storage: tp.Any = None
    _tasks: tp.List[tp.Awaitable] = field(default_factory=list)
    _to_close: tp.List[SupportsClose] = field(default_factory=list)

    @tp.overload
    def add_task(self, task: tp.Awaitable) -> None:
        ...

    @tp.overload
    def add_task(self, task: SupportsClose) -> None:
        ...

    def add_task(self, task) -> None:
        """
        Add task to be managed by the N23 framework.

        Task managed by the framework is closed on exit from `run` context
        manager.

        Task can be

        - an awaitable object
        - object with close method
        """
        is_task = inspect.isawaitable(task)
        to_close = hasattr(task, 'close')

        if not any([is_task, to_close]):
            raise ValueError('Task {} cannot be managed'.format(task))

        if is_task:
            self._tasks.append(task)

        if to_close:
            self._to_close.append(task)

    def close(self):
        """
        Close application context.

        Closes all managed tasks.
        """
        for t in self._to_close:
            try:
                t.close()
            except Exception as ex:
                logger.exception('Failed to close task: {}'.format(t))


@contextmanager
def run(interval: int, storage: tp.Optional[str]=None) -> tp.Iterator[Context]:
    """
    Run application based on N23 framework within the context of this
    function.

    The context manager provides the following benefits

    - installs uvloop based asyncio loop
    - creates scheduler
    - optionally, creates storage connection
    - installs TERM signal handler to allow graceful shutdown of
      application components
    """
    uvloop.install()
    loop = asyncio.get_event_loop()
    # shutdown gracefully on SIGTERM
    loop.add_signal_handler(signal.SIGTERM, sys.exit)

    try:
        scheduler = Scheduler(interval)
        ctx = Context(scheduler, from_uri(scheduler.ctx, storage))

        yield ctx

        tasks: tp.List[tp.Awaitable] = [ctx.scheduler]
        if ctx.storage:
            ctx.add_task(ctx.storage)

        tasks.extend(ctx._tasks)

        loop.run_until_complete(asyncio.gather(*tasks))
    finally:
        ctx.close()
        loop.remove_signal_handler(signal.SIGTERM)

def from_uri(scheduler_ctx, uri: tp.Optional[str]) -> object:
    """
    Create storage object or connection for N23 framework.
    """
    if not uri:
        return None

    v = urlparse(uri)
    f = STORAGE.get(v.scheme)
    if f is None:
        raise ApplicationError('Unknown storage: {}'.format(v.scheme))

    return f(scheduler_ctx, uri)

def tdb_from_uri(scheduler_ctx, uri: str):
    """
    Create TimescaleDB storage object using preparsed URI.
    """
    import n23.storage.tdb
    return n23.storage.tdb.Database(uri, scheduler_ctx)

STORAGE = {
    'postgresql': tdb_from_uri,
}

# vim: sw=4:et:ai
