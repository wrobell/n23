#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Implementation of default data storage for N23 framework.

The storage is backed by PostgreSQL with TimescaleDB extension (TimescaleDB in
short).

Features

- if database goes offline, the storage tries to reconnect until the database
  is back online
- will not run out of memory as data to be saved in a database is kept in
  a queue of limited size

The code shall serve as reference implementation for other backends, therefore
the API strives to be SQL/RDBMS independent. This might be challenging, so API
changes shall be expected.

N23 framework creates an instance of a database class, which is run
concurrently to the rest of application tasks. `Database.write` method is used
to enqueue data to be saved. The data is flushed to the database once per
tick of the scheduler, i.e. every second.

A table, which stores application data, needs to be declared with
`Database.add_entity` method. It allows to specify table columns and data queue
size.

Use `Database.add` method to register scheduler task handler within the storage
instance. The task handler name maps to a table and columns declared with
`Database.add_entity` method.

For example::

    db.add_entity('temperature`, ('location', 'value'))
    db.add(task_handler)  # `task_handler.name` is `temperature/room1`

then task task results::

    ('temperature/room1', 20.1)
    ('temperature/room1', 20.2)
    ('temperature/room1', 20.3)

are stored in table `temperature` as

    +----------+-------+
    | location | value |
    +----------+-------+
    | room1    |  20.1 |
    | room1    |  20.2 |
    | room1    |  20.3 |
    +----------+-------+

"""

import asyncio
import asyncpg
import cytoolz.functoolz as ftz
import logging
import typing as tp
from collections import deque
from contextlib import asynccontextmanager
from dataclasses import dataclass
from datetime import datetime
from functools import singledispatch, singledispatchmethod, partial
from urllib.parse import urlparse, urlunparse

from ...scheduler import Context, TaskHandle, TaskResult

logger = logging.getLogger(__name__)

T = tp.TypeVar('T')
TableColumns = tp.Sequence[str]
TableRow = tp.Tuple[tp.Any, ...]
Transform = tp.Callable[[TaskResult[T]], TableRow]
DatabaseTables = tp.Dict[str, 'TableDescriptor']
TableTransforms = tp.Dict[str, Transform]

# see https://github.com/MagicStack/asyncpg/issues/513
CONNECTION_ERRORS = (
    OSError,
    asyncpg.CannotConnectNowError,
    asyncpg.ConnectionDoesNotExistError,
)

DEFAULT_QUEUE_SIZE = 1024
CONNECTION_ERROR_BACK_TO_POOL = 'connection has been released back to the pool'
CONNECTION_ERROR_CLOSED = 'the underlying connection is closed'

@dataclass(frozen=True)
class TableDescriptor:
    """
    Define table name, its columns and function to prepare data for
    insertion.
    """
    table: str
    columns: TableColumns
    queue: tp.Deque
    save_queue: tp.Deque

def transform_default(result: TaskResult[T]) -> TableRow:
    return (result.value,)

class Database:
    def __init__(self, db_uri: str, ctx: Context):
        self.db_uri = db_uri
        self.ctx = ctx
        self.tables: DatabaseTables = {}
        self.transforms: TableTransforms = {}
        self.stop = False
        self.type_codecs: tp.Dict = {}

    def add_entity(
            self,
            name: str,
            columns: TableColumns,
            queue_size: int=DEFAULT_QUEUE_SIZE
        ):
        """
        Add and configure storage entity like database table.
        """
        if name in self.tables:
            raise ValueError('Table "{}" already defined' .format(name))
        if len(columns) < 1:
            raise ValueError('At least one column expected')

        self.tables[name] = TableDescriptor(
            name,
            ('time', *columns),
            deque([], maxlen=queue_size),
            deque([], maxlen=queue_size),
        )

    @singledispatchmethod
    def add(self, handle, transform: Transform[T]=transform_default) -> None:
        """
        Add table writer for a task.

        .. seealso:: `transform_default`

        :param handle: Task name or scheduler task handle.
        :param transform: Function to convert scheduler task result value into
            tuple of values to be saved in database table.
        """
        raise ValueError('Uknown handle type: {}'.format(type(handle)))

    @add.register
    def _add_name(
            self,
            handle: str,
            transform: Transform[T]=transform_default,
        ) -> None:

        table, *values = handle.split('/')

        if table not in self.tables:
            raise ValueError('Table "{}" not defined as entity'.format(table))

        if handle in self.transforms:
            raise ValueError('Task handle already exists')

        td = self.tables[table]

        assert len(td.columns) > len(values)

        self.transforms[handle] = partial(
            to_table_row, values=tuple(values), transform=transform
        )

    @add.register
    def _add_handle(
            self,
            handle: TaskHandle,
            transform: Transform[T]=transform_default,
        ) -> None:

        self.add(handle.name, transform)  # type: ignore

    def write(self, result: TaskResult) -> None:
        """
        Put task result data into a queue to be saved into a table.

        Table writer needs to be defined via `Database.add` method.

        :param result: Task result generated by N23 scheduler.
        """
        table, *_ = result.name.split('/', 1)
        td = self.tables[table]
        transform = self.transforms[result.name]
        data = transform(result)  # type: ignore
        td.queue.append(data)

    def set_type_codec(self, type_name, encoder) -> None:
        """
        Assign encoder to a database type.

        :param type_name: Database type name.
        :param encoder: Function converting Python value to format understood
            by database.
        """
        self.type_codecs[type_name] = encoder

    def __await__(self) -> tp.Generator[tp.Any, None, None]:
        yield from self._save_data().__await__()

    def close(self) -> None:
        self.stop = True

    async def _save_data(self) -> None:
        ctx = self.ctx
        async with create_pool(self.db_uri, self.type_codecs) as pool:
            while not self.stop:
                await ctx.on_tick.wait()

                task = save_data(pool, self.tables)
                await catch_conn_error(task)

            logger.info('save remaining data')
            task = save_data(pool, self.tables)
            await catch_conn_error(task)

def to_table_row(
            result: TaskResult[T],
            values: TableRow,
            transform: Transform[T],
        ) -> TableRow:

    return (datetime.utcfromtimestamp(result.time),) \
        + values \
        + transform(result)

async def catch_conn_error(coro: tp.Coroutine) -> None:
    """
    Protect calling coroutine from connection error of coroutine being
    executed.

    :param coro: Coroutine to execute.
    """
    try:
        await coro
    except CONNECTION_ERRORS as ex:
        logger.warning(
            'connection failure due to: {}'.format(ex)
        )
    except asyncpg.InterfaceError as ex:
        if not is_disconnected(ex):
            raise
        logger.warning(
            'connection failure due to: {}'.format(ex)
        )

async def save_data(pool: asyncpg.pool.Pool, tables: DatabaseTables):
    items = (td for td in tables.values() if td.queue)
    tasks = (save_table_data(pool, td) for td in items)

    # upload data for each table in parallel, using one transaction per
    # table; if upload for one table fails, the data for other tables is
    # still stored; the connection pool controls how many parallel
    # connections are performed
    await asyncio.gather(*tasks)

async def save_table_data(pool: asyncpg.pool.Pool, td: TableDescriptor):
    """
    Save data from a queue into a database table.

    The saved elements are removed from the queue.

    :param pool: Database connection pool.
    :param td: Table information.
    """
    # the n23 scheduler can modify a queue keeping data for a table at any
    # time; an error is raised if the queue is being iterated by asyncpg at
    # the same time; store data in the save queue to avoid the problem
    td.save_queue.extend(td.queue)
    td.queue.clear()

    async with connect(pool) as conn:
        await conn.copy_records_to_table(
            td.table,
            records=td.save_queue,
            columns=td.columns,
        )
        n = len(td.save_queue)
        td.save_queue.clear()
        logger.info('stored {} records in table {}'.format(n, td.table))

@asynccontextmanager
async def create_pool(db_uri: str, type_codecs: tp.Dict={}, max_size: int=1):
    """
    Create connection pool for a database.

    :param db_uri: Database URI.
    :param type_codecs: Mapping between database types and their encoders.
    :param max_size: Maximum number of connections of connection pool.
    """
    async with asyncpg.create_pool(
                db_uri,
                min_size=1,
                max_size=max_size,
                init=partial(set_connection_codecs, type_codecs),
            ) as pool:

        yield pool

@asynccontextmanager
@singledispatch
async def connect(
        db,
        type_codecs={},
        max_size=1,
        enable_tx=True
    ):
    """
    Context manager to connect to a database.

    First parameter can be

    - database URI
    - or database connection pool

    In first instance, new connection pool is created to connect to
    a database. In second instance, the connection pool is reused to
    connect to a database.

    :param db: Database URI or database connection pool.
    :param max_size: Maximum number of connections of connection pool.
    :param enable_tx: Create database transaction (true by default).
    """
    raise NotImplementedError(
        'Not implemented for database parameter type: {}'
        .format(type(db))
    )

@asynccontextmanager
@connect.register(str)  # type: ignore
async def _connect_db_uri(
        db_uri,
        type_codecs={},
        max_size=1,
        enable_tx=True
    ):
    """
    Create connection pool and use it to connect to a database.
    """
    async with create_pool(db_uri, type_codecs, max_size=max_size) as pool:
        async with connect(pool, enable_tx=enable_tx) as conn:
            yield conn

@asynccontextmanager
@connect.register(asyncpg.pool.Pool)  # type: ignore
async def _connect_db_pool(pool, enable_tx=True):
    """
    Reuse existing connection pool to connect to a database.
    """
    async with pool.acquire() as conn:
        if enable_tx:
            async with conn.transaction():
                yield conn
        else:
            yield conn

async def set_connection_codecs(type_codecs, conn):
    set_codec = partial(
        conn.set_type_codec, decoder=ftz.identity, format='binary'
    )
    for t, f in type_codecs.items():
        if __debug__:
            logger.debug('set codec for type {}: {}'.format(t, f))
        await set_codec(t, encoder=f)

async def execute(db_uri, cmd, enable_tx=True):
    """
    Connect to database and execute single database statement.
    """
    logger.info('execute: {}'.format(cmd))
    async with connect(db_uri, enable_tx=enable_tx) as conn:
        await conn.execute(cmd)

def is_disconnected(ex: asyncpg.InterfaceError) -> bool:
    """
    Check if the PostgreSQL interface error is a disconnection error.
    """
    message = str(ex)
    disconnected = CONNECTION_ERROR_BACK_TO_POOL in message \
        or CONNECTION_ERROR_CLOSED in message
    logger.debug(
        'interface error (disconnected={}): {}'
        .format(disconnected, message)
    )
    return disconnected

def to_db_uri(db: str, alt_db: tp.Optional[str]=None) -> str:
    """
    Convert database name or partial database connection string to database
    connection string for local database server.

    If the `db` parameter is database connection string already, then do
    nothing.

    For example::

        some-db -> postgresql://localhost/some-db
        localhost/some-db -> postgresql://localhost/some-db
        postgresql://localhost/some-db -> postgresql://localhost/some-db
        user:pass@localhost/some-db -> postgresql://user:pass@localhost/some-db

    Use alternative database name to get connection for template database,
    i.e. `template1`.

    @param db: Database name or database connection string.
    @param alt_db: Alternative database name.
    """
    db = db if db.startswith('postgresql://') or db.startswith('//') else '//' + db
    db_uri = urlparse(db, scheme='postgresql')

    # if no hostname in `db`, then netloc points to dbname, therefore make
    # a swap and use localhost for hostname
    host, dbname = (db_uri.netloc, db_uri.path) \
        if db_uri.netloc and db_uri.path \
        else ('localhost', db_uri.netloc)
    dbname = dbname if alt_db is None else alt_db

    db_uri = db_uri._replace(path=dbname, netloc=host)
    logger.info('parsed db uri: {}'.format(db_uri))
    return urlunparse(db_uri)

# vim: sw=4:et:ai
