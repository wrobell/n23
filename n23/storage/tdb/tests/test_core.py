#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for basic database functions.
"""

import asyncpg
from collections import deque
from contextlib import contextmanager
from datetime import datetime

from .. import core as tdb_core
from ....scheduler import TaskHandle, TaskResult

import pytest
from unittest import mock

@contextmanager
def patch_connect(side_effect=None):
    """
    Context manager to patch `connect` coroutine.
    """
    with mock.patch.object(tdb_core, 'connect', mock.MagicMock()) as mock_connect:
        conn = mock.AsyncMock(spec=asyncpg.Connection)
        task = mock_connect()
        task.__aenter__.return_value = conn
        task.__aenter__.side_effect = side_effect

        yield conn

def test_db_add_name():
    """
    Test adding table writer.
    """
    db = tdb_core.Database('test', mock.MagicMock())
    db.add_entity('t1', ('value',))
    db.add('t1')

    assert 't1' in db.tables
    assert 't1' in db.transforms

    td = db.tables['t1']
    assert 't1' == td.table
    assert ('time', 'value') == td.columns

def test_db_add_handle():
    """
    Test adding table writer.
    """
    th = TaskHandle('t1', False)
    db = tdb_core.Database('test', mock.MagicMock())
    db.add_entity('t1', ('value',))
    db.add(th)

    assert 't1' in db.tables
    assert 't1' in db.transforms

    td = db.tables['t1']
    assert 't1' == td.table
    assert ('time', 'value') == td.columns

def test_db_add_with_measure():
    """
    Test adding table writer with a measure column.
    """
    th = TaskHandle('t1/room1', False)
    db = tdb_core.Database('test', mock.MagicMock())
    db.add_entity('t1', ('name', 'value'))
    db.add(th)

    assert 't1' in db.tables
    assert 't1/room1' in db.transforms

    td = db.tables['t1']
    assert 't1' == td.table
    assert ('time', 'name', 'value') == td.columns

def test_db_add_existing_handle():
    """
    Test adding table writer while one exists already.
    """
    th = TaskHandle('t1', False)
    db = tdb_core.Database('test', mock.MagicMock())
    db.add_entity('t1', ('value',))
    db.add(th)
    with pytest.raises(ValueError):
        db.add(th)

def test_db_write():
    """
    Test writing data.
    """
    th = TaskHandle('t1/room1', False)
    db = tdb_core.Database('test', mock.MagicMock())
    db.add_entity('t1', ('name', 'value'))
    db.add(th)

    v1 = TaskResult(1, 't1/room1', 'v1')
    v2 = TaskResult(2, 't1/room1', 'v2')

    db.write(v1)
    db.write(v2)

    expected = (
        (datetime(1970, 1, 1, 0, 0, 1), 'room1', 'v1'),
        (datetime(1970, 1, 1, 0, 0, 2), 'room1', 'v2'),
    )
    assert expected == tuple(db.tables['t1'].queue)

@pytest.mark.asyncio
async def test_save_table_data():
    """
    Test saving data into a table.
    """
    pool = mock.MagicMock()
    td = tdb_core.TableDescriptor(
        't1',
        ('a', 'b'),
        deque([1, 2, 3, 4], maxlen=5),
        deque([], maxlen=5),
    )

    with patch_connect() as conn:
        async def side_effect(*args, **kw):
            assert (td.table,) == args
            assert deque([1, 2, 3, 4], maxlen=5) == kw['records']
            assert td.columns == kw['columns']

        conn.copy_records_to_table = mock.AsyncMock(side_effect=side_effect)
        await tdb_core.save_table_data(pool, td)
        conn.copy_records_to_table.assert_called_once()

        # both the main queue and save queue are cleared after data is
        # saved into the table
        assert 0 == len(td.queue)
        assert 0 == len(td.save_queue)

@pytest.mark.asyncio
async def test_save_table_data_error():
    """
    Test if data is kept in save queue on data save failure.
    """
    pool = mock.MagicMock()
    td = tdb_core.TableDescriptor(
        't1',
        ('a', 'b'),
        deque([1, 2, 3, 4], maxlen=5),
        deque([10, 11, 12, 13], maxlen=5),
    )

    with patch_connect(side_effect=ValueError) as conn:
        try:
            await tdb_core.save_table_data(pool, td)
        except ValueError:
            assert [] == list(td.queue)
            assert [13, 1, 2, 3, 4] == list(td.save_queue)
        else:
            assert False, 'ValueError exception expected'

def test_is_disconnected_closed():
    """
    Test if PostgreSQL interface error for closed connection can be
    detected.
    """
    ex = asyncpg.InterfaceError('...: the underlying connection is closed')
    assert tdb_core.is_disconnected(ex)

def test_is_disconnected_back_to_pool():
    """
    Test if PostgreSQL interface error for connection released to
    connection pool is detected.
    """
    ex = asyncpg.InterfaceError(
        '...: connection has been released back to the pool'
    )
    assert tdb_core.is_disconnected(ex)

def test_db_uri_scheme_host():
    """
    Test adding db URI scheme and host.
    """
    result = tdb_core.to_db_uri('some-db')
    assert 'postgresql://localhost/some-db' == result

def test_db_uri_scheme_host_with_user():
    """
    Test adding db URI scheme and host for db with user.
    """
    result = tdb_core.to_db_uri('user:pass@localhost/some-db')
    assert 'postgresql://user:pass@localhost/some-db' == result

def test_db_uri_scheme():
    """
    Test adding db URI scheme.
    """
    result = tdb_core.to_db_uri('10.0.0.1/some-db')
    assert 'postgresql://10.0.0.1/some-db' == result

def test_db_uri_dbname():
    """
    Test replacing db URI database name.
    """
    result = tdb_core.to_db_uri('postgresql://localhost/some-db', alt_db='other-name')
    assert 'postgresql://localhost/other-name' == result

@pytest.mark.asyncio
async def test_connect_db_pool():
    """
    Test connecting to database with existing connection pool.
    """
    conn = mock.MagicMock()
    pool = mock.MagicMock()
    pool.acquire.return_value = mock.MagicMock()
    pool.acquire.return_value.__aenter__.return_value = conn
    async with tdb_core._connect_db_pool(pool) as conn:
        pass

    # check that connection is acquired using the connection pool
    pool.acquire.assert_called_once_with()

    # check that transaction is created
    conn.transaction.assert_called_once_with()

@pytest.mark.asyncio
async def test_catch_conn_error_from_pool():
    """
    Test catching connection error raised by connection pool.
    """
    task = mock.AsyncMock(side_effect=[OSError])
    await tdb_core.catch_conn_error(task())
    # no exception as raised error is connection error
    assert True

@pytest.mark.asyncio
async def test_catch_conn_error_on_exec():
    """
    Test catching connection error raised during command execution.
    """
    ex = asyncpg.InterfaceError('...: the underlying connection is closed')
    task = mock.AsyncMock(side_effect=[ex])
    await tdb_core.catch_conn_error(task())
    # no exception as raised error is connection error
    assert True

@pytest.mark.asyncio
async def test_catch_conn_error_fail():
    """
    Test exceptions other than connection error are not swallowed when
    protecting from connection errors.
    """
    task = mock.AsyncMock(side_effect=[ValueError()])
    with pytest.raises(ValueError) as ex_ctx:
        await tdb_core.catch_conn_error(task())

@pytest.mark.asyncio
async def test_create_pool(mocker):
    """
    Test creating connection pool.
    """
    mock_create_pool = mocker.patch('asyncpg.create_pool')
    mock_create_pool().__aenter__ = mock.AsyncMock()
    mock_create_pool().__aexit__ = mock.AsyncMock()

    async with tdb_core.create_pool('test') as pool:
        mock_create_pool.assert_called_once_with(
            'test', min_size=1, max_size=1
        )

# vim: sw=4:et:ai
