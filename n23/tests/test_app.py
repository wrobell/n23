#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for functions supporting running of applications based on N23
framework.
"""

from urllib.parse import urlparse, ParseResult

from ..app import Context, from_uri, tdb_from_uri, ApplicationError

import pytest
from unittest import mock


def test_add_task_to_manage():
    """
    Test adding a task to N23 framework application context.
    """
    ctx = Context(mock.MagicMock())
    async def f(): pass

    t = f()
    ctx.add_task(t)
    assert t in ctx._tasks

def test_add_task_to_close():
    """
    Test adding a task to N23 framework application context.
    """
    ctx = Context(mock.MagicMock())
    t = mock.MagicMock()
    t.close = lambda v: v
    ctx.add_task(t)
    assert t in ctx._to_close

def test_add_task_invalid():
    """
    Test adding an invalid task to N23 framework application context.
    """
    ctx = Context(mock.MagicMock())

    with pytest.raises(ValueError) as ctx_ex:
        ctx.add_task(int)

def test_app_ctx_close():
    """
    Test closing tasks by N23 framework application context.
    """
    class T:
        def __init__(self):
            self.closed = False

        def close(self):
            self.closed = True

    ctx = Context(mock.MagicMock())
    t1 = T()
    t2 = T()
    t3 = T()

    t2.close = mock.MagicMock(side_effect=KeyError)

    ctx.add_task(t1)
    ctx.add_task(t2)
    ctx.add_task(t3)

    with mock.patch('n23.app.logger') as mock_log:
        ctx.close()
        mock_log.exception.assert_called_once()

def test_from_uri_error():
    """
    Test if application error is raised when storage is not recognized.
    """
    with pytest.raises(ApplicationError) as ex_ctx:
        ctx = mock.MagicMock()
        from_uri(ctx, 'http://invalid')
    assert 'Unknown storage: http' == str(ex_ctx.value)

def test_tdb_from_uri():
    """
    Test creating TimescaleDB storage instance from URI.
    """
    uri = 'postgresql://localhost/dbname'
    ctx = mock.MagicMock()
    db = tdb_from_uri(ctx, urlparse(uri))
    assert ctx == db.ctx

    expected = ParseResult(
        scheme='postgresql',
        netloc='localhost',
        path='/dbname',
        params='',
        query='',
        fragment='',
    )
    assert expected == db.db_uri

# vim: sw=4:et:ai
