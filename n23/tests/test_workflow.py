#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for N23 framework workflow operators.
"""

from n23 import workflow

from unittest import mock

def test_split():
    """
    Test split workflow operator.
    """
    f1 = mock.MagicMock()
    f2 = mock.MagicMock()
    f = workflow.split(f1, f2)
    f('a')
    f1.assert_called_once_with('a')
    f2.assert_called_once_with('a')

def test_pipe():
    """
    Test pipe workflow operator.
    """
    predicate = lambda v: v % 2 == 0
    f1 = lambda v: v + 5
    f2 = lambda v: v // 2
    f = workflow.pipe(f1, f2)

    result = f(3)  # (3 + 5) // 2 == 4
    assert 4 == result

def test_filter():
    """
    Test filter workflow operator.
    """
    predicate = lambda v: v % 2 == 0
    f1 = lambda v: v + 2
    f2 = lambda v: v // 2
    sink = mock.MagicMock()
    f = workflow.filter(predicate, f1, f2, sink)

    f.send(3)  # 3 % 2 == 1
    assert not sink.called

    f.send(4)  # 4 % 2 == 0
    sink.assert_called_once_with(3)

def test_to_call_function():
    """"
    Test extracting callable function.
    """
    def f():
        pass

    result = workflow.to_call(f)
    assert f is result

def test_to_call_generator():
    """"
    Test extracting callable function from generator coroutine.
    """
    def coro():
        v = yield

    c = coro()
    result = workflow.to_call(c)
    assert c.send == result

def test_calls():
    """
    Test extracting callables.
    """
    f1 = lambda v: v + 2
    f2 = lambda v: v / 2

    # nulls to be skipped
    rf1, rf2 = workflow.calls(f1, None, f2, None)
    assert f1 == rf1
    assert f2 == rf2

# vim: sw=4:et:ai
