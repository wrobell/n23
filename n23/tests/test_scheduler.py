#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
The n23 scheduler unit tests.
"""

import asyncio
import functools
import logging
import time

from ..timer import Timer
from ..scheduler import Scheduler, TaskHandle, Task, BgTask, logger as s_logger

import pytest
from unittest import mock

logger = logging.getLogger()

def test_scheduler_default_timeout():
    """
    Test scheduler default timeout calculation.
    """
    scheduler = Scheduler(1)
    assert 0.25 == scheduler.timeout

@pytest.mark.asyncio
async def test_task_read_func():
    """
    Test reading data with a function.
    """
    async def mock_executor(_, f):
        return f()

    result = {}
    callback = functools.partial(result.__setitem__, 'result')

    reader = lambda: 'value'
    loop = mock.MagicMock()
    loop.run_in_executor = mock_executor

    th = TaskHandle('pressure', True)
    task = Task(th, reader, callback)

    await task
    assert 'pressure' == result['result'].name
    assert 'value' == result['result'].value

@pytest.mark.asyncio
async def test_task_read_coroutine():
    """
    Test reading data with a coroutine.
    """
    async def reader():
        return 'value'

    result = {}
    callback = functools.partial(result.__setitem__, 'result')

    th = TaskHandle('pressure', True)
    task = Task(th, reader, callback)

    await task
    assert 'pressure' == result['result'].name
    assert 'value' == result['result'].value

@pytest.mark.timeout(1)
@pytest.mark.asyncio
async def test_coroutine_task_cancel():
    """
    Test cancellation of a scheduler task running a coroutine.
    """
    async def reader():
        await asyncio.sleep(5)
        return 'value'

    callback = mock.MagicMock()
    th = TaskHandle('pressure', True)
    task = Task(th, reader, callback)

    try:
        # note: task is cancelled by `wait_for` on timeout
        await asyncio.wait_for(task, timeout=0.1)
    except asyncio.TimeoutError as ex:
        logger.warning('ignoring: {}'.format(ex))

    # sucessfully cancelled and no result
    assert task.task is None
    assert not callback.called

@pytest.mark.timeout(1)
@pytest.mark.asyncio
async def test_async_generator_task_cancel():
    """
    Test cancellation of a scheduler task running an asynchronous generator.
    """
    async def reader():
        while True:
            await asyncio.sleep(0.5)
            yield 'value'

    callback = mock.MagicMock()
    th = TaskHandle('pressure', True)
    task = Task(th, reader, callback)

    try:
        # note: task is cancelled by `wait_for` on timeout
        await asyncio.wait_for(task, timeout=0.1)
    except asyncio.TimeoutError as ex:
        logger.warning('ignoring: {}'.format(ex))

    # sucessfully cancelled and no result
    assert task.task is None
    assert not callback.called

@pytest.mark.timeout(1)
@pytest.mark.asyncio
async def test_func_task_cancel():
    """
    Test cancellation of a scheduler task running a function.
    """
    def reader():
        time.sleep(0.5)
        return 'value'

    callback = mock.MagicMock()
    th = TaskHandle('pressure', True)
    task = Task(th, reader, callback)

    try:
        await asyncio.wait_for(task, timeout=0.1)
    except asyncio.TimeoutError as ex:
        logger.warning('ignoring: {}'.format(ex))

    # sucessfully cancelled and no result
    assert task.task is None
    assert not callback.called

@pytest.mark.asyncio
async def test_task_bg_read():
    """
    Test reading data with background task.
    """
    def callback(data):
        for i in range(5):
            tr = yield
            data.append(tr)
        raise asyncio.CancelledError()

    async def reader():
        return 'value'

    result = []
    c = callback(result)
    next(c)
    task = BgTask(TaskHandle('pressure', False), reader, c.send)

    timer = mock.MagicMock()
    timer._tick = 4
    try:
        await task
    except asyncio.CancelledError as ex:
        logger.warning('ignoring: {}'.format(ex))
    assert ['pressure'] * 5 == [v.name for v in result]
    assert ['value'] * 5 == [v.value for v in result]

@pytest.mark.asyncio
async def test_task_read_coroutine_partial():
    """
    Test reading data with coroutine enclosed with partial.
    """
    async def reader(v):
        return 'value ' + v

    result = {}
    callback = functools.partial(result.__setitem__, 'result')

    reader = functools.partial(reader, 'test')
    th = TaskHandle('pressure', True)
    task = Task(th, reader, callback)

    await task
    assert 'value test' == result['result'].value

def test_scheduler_adding_regular_task():
    """
    Test adding task and its workflow callback to scheduler.
    """
    async def f1():
        return 1

    f2 = lambda: 1

    scheduler = Scheduler(1)
    handle = scheduler.add('pressure', f1, f2)
    assert not scheduler._bg_tasks
    task = scheduler._tasks[0]
    assert handle == task.handle
    assert f2 == task.callback

def test_scheduler_adding_bg_task():
    """
    Test adding event-driven task and its workflow callback to scheduler.
    """
    async def f1():
        return 1

    f2 = lambda: 1

    scheduler = Scheduler(1)
    handle = scheduler.add('pressure', f1, f2, regular=False)
    assert not scheduler._tasks
    task = scheduler._bg_tasks[0]
    assert handle == task.handle
    assert f2 == task.callback

@pytest.mark.timeout(5)
@pytest.mark.asyncio
async def test_scheduler_reader_regular():
    """
    Test scheduler reading data with a coroutine.
    """
    async def reader():
        return 2

    callback = mock.MagicMock(side_effect=[None, ValueError('stop')])

    start = time.time()
    scheduler = Scheduler(0.1)
    scheduler.add('pressure', reader, callback)
    try:
        await scheduler._run_tasks()
    except ValueError as ex:
        logger.warning('ignoring: {}'.format(ex))

    assert 1 == scheduler.ctx.tick
    assert start < scheduler.ctx.time

@pytest.mark.asyncio
async def test_scheduler_reader_error():
    """
    Test scheduler not swallowing error thrown by data reader.
    """
    async def reader():
        raise ValueError('test test test')

    scheduler = Scheduler(0.1)
    scheduler.add('pressure', reader, lambda: 1)
    with pytest.raises(ValueError) as ctx:
        await scheduler._run_tasks()

    assert 'test test test' == str(ctx.value)

@pytest.mark.timeout(1)
@pytest.mark.asyncio
async def test_scheduler_reader_timeout():
    """
    Test scheduler logging warning on data read timeout.
    """
    with mock.patch.object(asyncio, 'wait') as mock_wait:
        task = mock.MagicMock()
        task.get_name.return_value = 'pressure'

        stopper = mock.MagicMock()
        stopper.exception.return_value = ValueError('to be ignored')
        mock_wait.side_effect = [
            [{}, {task}],
            [{stopper}, {}],
        ]

        scheduler = Scheduler(0.1)
        scheduler.add('pressure', mock.MagicMock(), mock.MagicMock())
        p_logger = mock.patch.object(s_logger, 'warning')
        with p_logger as f:
            try:
                await scheduler._run_tasks()
            except ValueError as ex:
                logger.warning('ignoring: {}'.format(ex))

            # the timeout was logged and task got cancelled
            f.assert_called_once_with('task pressure timeout')
            task.cancel.assert_called_once_with()

@pytest.mark.asyncio
async def test_scheduler_workflow_error():
    """
    Test scheduler not swallowing error thrown by workflow callback.
    """
    async def reader():
        return 1

    callback = mock.MagicMock()
    callback.side_effect = [ValueError('test test test')]

    scheduler = Scheduler(0.1)
    scheduler.add('pressure', reader, callback)
    assert len(scheduler._tasks) == 1

    with pytest.raises(ValueError) as ctx:
        await scheduler._run_tasks()

    assert 'test test test' == str(ctx.value)
    callback.assert_called_once()

# vim: sw=4:et:ai
