#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Asynchronous task scheduler.
"""

import asyncio
import cytoolz.functoolz as ftz
import inspect
import logging
import time
import typing as tp
from dataclasses import dataclass
from functools import partial

from .timer import Timer

logger = logging.getLogger(__name__)

T = tp.TypeVar('T')

CallDataSource = tp.Callable[[], T]

@dataclass(frozen=True)
class TaskHandle(tp.Generic[T]):
    """
    Scheduler task handle.
    """
    __slots__ = ['name', 'regular']

    name: str
    regular: bool

@dataclass(frozen=True)
class TaskResult(tp.Generic[T]):
    """
    Result of task execution.
    """
    __slots__ = ['time', 'name', 'value']

    time: float
    name: str
    value: T

@dataclass(frozen=False)
class Context:
    """
    Result of task execution.
    """
    __slots__ = ['interval', 'tick', 'time', 'on_tick']

    # scheduler interval
    interval: float

    # timer cycle tick
    tick: int

    # time at the start of timer cycle
    time: float

    # event triggered when all tasks completed in given timer cycle
    on_tick: asyncio.Event

class Scheduler:
    """
    Asynchronous task scheduler.

    :var ctx: Scheduler context.
    :var timeout: Asynchronous task timeout.
    """
    def __init__(self, interval, timeout=None):

        if timeout is None:
            timeout = interval * 0.25
        self.timeout = timeout

        self._tasks: tp.List[Task] = []
        self._bg_tasks: tp.List[BgTask] = []

        timer = self.timer = Timer(interval)
        self.ctx = Context(interval, timer._tick, time.time(), asyncio.Event())

    def add(
            self,
            name: str,
            func: CallDataSource,
            callback: tp.Callable[[TaskResult[T]], tp.Any],
            *,
            regular=True
        ) -> TaskHandle[T]:
        """
        Add an asynchronous task to the scheduler.

        Tasks are executed at regular intervals with timer or on the
        event-driven basis in the background. Callback is executed when
        task receives data.

        Callback is executed with task result as its first parameter. Use
        `functools.partial` to set default parameters if callback has more
        parameters.

        If `regular` is set to true, then asynchronous task is run at
        regular intervals with a timer, thus obtaining equally spaced
        time series. Otherwise, task is run in background and unevenly
        spaced time series of data is generated.

        :param name: Name (identifier) of the task.
        :param func: Coroutine or function to execute as task.
        :param callback: Callback to process task data.
        :param regular: Task is run at regular intervals if true (default).
        """
        if regular:
            cls = Task
            tasks = self._tasks
        else:
            cls = BgTask            # type: ignore
            tasks = self._bg_tasks  # type: ignore
        handle = TaskHandle[T](name, regular)
        task = cls(handle, func, callback)
        tasks.append(task)
        return handle

    def __await__(self):
        """
        Start scheduler.
        """
        # always run the regular tasks loop, even if there are not tasks, so
        # the scheduler tick is generated
        tasks = [self._run_tasks()]

        # add background tasks
        tasks.extend(t for t in self._bg_tasks)

        yield from asyncio.gather(*tasks).__await__()

    async def _run_tasks(self) -> None:
        timer = self.timer
        ctx = self.ctx
        on_tick = ctx.on_tick
        wait_tasks = partial(
            asyncio.wait,
            timeout=self.timeout,
            return_when=asyncio.FIRST_EXCEPTION
        )
        create_task = lambda t: asyncio.create_task(
            t.__await__(),
            name=t.handle.name
        )
        try:
            timer.start()
            while True:
                await timer

                ctx.time = time.time()
                ctx.tick = timer._tick

                if self._tasks:
                    # start tasks; after timeout, the pending tasks are
                    # cancelled
                    tasks = [create_task(t) for t in self._tasks]
                    done, pending = await wait_tasks(tasks)

                    # crash software on error
                    error = next((e for t in done if (e := t.exception())), None)
                    if error:
                        raise error

                    for t in pending:
                        logger.warning('task {} timeout'.format(t.get_name()))  # type: ignore
                        # try to cancel task to make it available again
                        t.cancel()

                on_tick.set()
                on_tick.clear()
        finally:
            timer.close()

class TaskBase:
    """
    Abstract class representing asynchronous task executed by the
    scheduler.

    :var handle: Scheduler task handle.
    :var func: Function to read data.
    :var callback: Callback to process task data.
    :var _runner: Function to start asynchronous coroutine executing
        a task.

    .. seealso:: Scheduler.add
    """
    def __init__(self, handle: TaskHandle[T], func, callback):
        self.handle = handle
        self.func = func
        self.callback = callback

        create_task = asyncio.create_task
        f = func.func if isinstance(func, partial) else func
        if inspect.iscoroutinefunction(f):
            self._runner = ftz.compose(create_task, func)
            logger.info('{} is coroutine function'.format(func))
        elif inspect.isasyncgenfunction(f):
            self._runner = ftz.compose(create_task, func().__anext__)
            logger.info('{} is asynchronous generator'.format(func))
        else:
            loop = asyncio.get_event_loop()
            self._runner = partial(loop.run_in_executor, None, func)
            logger.info('{} is function, will be run in executor' .format(func))

    async def _execute_task(self, task) -> None:
        """
        Execute task and call task callback with task result as parameter.

        If task execution is cancelled, then the cancellation error is
        ignored.
        """
        try:
            result = await task
        except asyncio.CancelledError as ex:
            logger.warning('task {} is cancelled'.format(self.handle.name))
        else:
            tr = TaskResult(time.time(), self.handle.name, result)
            self.callback(tr)

class Task(TaskBase):
    """
    Task scheduled at regular intervals.
    """
    def __init__(self, handle: TaskHandle[T], func, callback):
        super().__init__(handle, func, callback)
        self.task = None

    def __await__(self) -> tp.Generator[None, None, None]:
        """
        Execute task.
        """
        self.task = self._runner()
        yield from self._execute_task(self.task).__await__()
        self.task = None

    def cancel(self) -> None:
        """
        Cancel running task.
        """
        if self.task is not None:
            self.task.cancel()
        self.task = None

class BgTask(TaskBase):
    """
    Run task in background.
    """
    def __await__(self) -> tp.Generator[None, None, None]:
        """
        Execute task.
        """
        while True:
            yield from self._execute_task(self._runner()).__await__()

# vim: sw=4:et:ai
