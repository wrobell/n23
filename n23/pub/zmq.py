#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Publish and receive data from ZeroMQ topic.

The data is encoded in MessagePack format.
"""

import zmq
import zmq.asyncio
import msgpack

def server(address: str):
    """
    Create ZeroMQ publisher socket.

    Note: the socket is regular, this is non-asyncio socket.

    :param address: ZeroMQ socket address.
    """
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind(address)
    return socket

def subscribe(address: str, topic: str):
    """
    Create ZeroMQ subscriber topic and subscribe to the specified topic.

    Note: the socket is asyncio socket.

    :param address: ZeroMQ socket address.
    :param topic: ZeroMQ socket topic.
    """
    context = zmq.asyncio.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(address)
    socket.setsockopt(zmq.SUBSCRIBE, topic.encode())

    return socket

def send_nowait(socket, topic: str, data: object):
    """
    Send data over ZeroMQ publisher socket.

    The data is encoded in MessagePack format.

    :param socket: ZeroMQ publisher socket.
    :param topic: ZeroMQ socket topic.
    :param data: Data to be sent over the socket.
    """
    msg = bytearray(topic.encode())
    msg.append(0)
    msg.extend(msgpack.packb(data))
    socket.send(msg)

async def recv(socket):
    """
    Receive data from ZeroMQ subscriber socket.

    The received data is decoded with MessagePack library.

    :param socket: ZeroMQ subscriber socket.
    """
    msg = await socket.recv()
    topic, data = msg.split(b'\x00', 1)
    return msgpack.unpackb(data, encoding='utf-8')

# vim: sw=4:et:ai
