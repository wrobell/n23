#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


"""
Data workflow operators.

The following operators are supported

split
    Create a function to call multiple functions with the same value.
pipe
    Pipe a value through a collection of functions.
filter
    Call function with a value if value satisfies a predicate.

Collection of functions is passed to an operator and any function can be
null. This allows to configure data workflows, for example::

    f = split(
        publish,  # always publish value
        print if debug else None,  # print to stdout only if debug enabled
    )
"""

import cytoolz.functoolz as ftz
import inspect
import logging
import typing as tp
from functools import wraps

logger = logging.getLogger(__name__)

S = tp.TypeVar('S')
T = tp.TypeVar('T')

# workflow callable accepts single argument
Callable = tp.Callable[[T], S]

# workflow generators accept only a value via its `send` method, they
# return nothing
Generator = tp.Generator[None, T, tp.NoReturn]
FGenerator = tp.Callable[..., Generator[T]]


def coroutine(func: FGenerator[T]) -> FGenerator[T]:
    """
    Decorator to define workflow coroutine.
    """
    @wraps(func)
    def start(*args, **kwargs) -> Generator[T]:
        cr: Generator[T] = func(*args, **kwargs)
        next(cr)
        return cr
    return start

def split(*func):
    """
    Split workflow operator.

    Create function to call provided callables with the same argument.

    :param func: List of functions.

    .. seealso:: :py:func:`n23.util.to_call`
    """
    return ftz.juxt(*calls(*func))

def pipe(*func):
    """
    Pipe workflow operator.

    Create function composed of provided functions.

    :param func: Collection of functions.

    .. seealso:: :py:func:`n23.util.to_call`
    """
    items = list(calls(*func))
    return ftz.compose(*reversed(items))

@coroutine
def filter(predicate: Callable[T, bool], *func) -> Generator[T]:
    """
    Filter workflow operator.

    Create coroutine to filter received values. If value is to be accepted
    by predicate, then the value is piped through the provided functions.

    :param predicate: Predicate, the filter function.
    :param func: Functions forming the pipe.
    """
    fp = pipe(*func)
    while True:
        value = yield
        if predicate(value):
            fp(value)

def calls(*func: tp.Optional[Callable]) -> tp.Iterator[Callable]:
    """
    Return collection of callables extracted with `to_call` function.

    If an item of `funcs` is null, then skip it.
    """
    yield from (to_call(f) for f in func if f is not None)

@tp.overload
def to_call(f: Callable[T, S]) -> Callable[T, S]: ...

@tp.overload
def to_call(f: Generator[T]) -> Callable[T, tp.NoReturn]: ...

def to_call(f):
    """
    Extract callable from `f`.

    The `f` can be

    - function or method, it is returned directly
    - generator coroutine, its `send` method is returned

    :param f: Function or an object to extract callable from.
    """
    return f.send if inspect.isgenerator(f) else f

# vim: sw=4:et:ai
