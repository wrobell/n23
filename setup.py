#!/usr/bin/env python3
#
# n23 - data acquisition and processing framework
#
# Copyright (C) 2013-2020 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os.path

from setuptools import setup, find_packages

setup(
    name='n23',
    version='0.1.0',
    description='n23 - data acquisition and processing framework',
    author='Artur Wroblewski',
    author_email='wrobell@riseup.net',
    url='https://bitbucket.org/wrobell/n23',
    setup_requires = ['setuptools_git >= 1.0',],
    packages=find_packages('.'),
    scripts=(
        'bin/n23db-db',
        'bin/n23db-table',
        'bin/n23db-column',
        'bin/n23db-sync',
    ),
    include_package_data=True,
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3',
        'Development Status :: 2 - Pre-Alpha',
    ],
    install_requires=[
        'asyncpg', 'atimer', 'cytoolz', 'uvloop'
    ],
    license='GPLv3+',
    long_description=open('README').read(),
    long_description_content_type='text/x-rst',
)

# vim: sw=4:et:ai
